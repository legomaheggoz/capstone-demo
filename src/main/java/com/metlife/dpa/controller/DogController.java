package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Dog;

@Controller
public class DogController {

	@RequestMapping(value="/dog")
	public String sayHello(Model model){
		//model.addAttribute("greeting", "Hello world!");
		return "index";
	}
	
    @RequestMapping(value="/dog", method=RequestMethod.GET)
    public String dogForm(Model model) {
        model.addAttribute("formDog", new Dog());
        return "dog";
    }

    @RequestMapping(value="/result", method=RequestMethod.POST)
    public String dogSubmit(@ModelAttribute Dog dog, Model model) {
    	//System.out.println(dog.getName());
    	//System.out.println(dog.getAge());
    	//System.out.println(dog.getBreed());
        model.addAttribute("dog", dog);
        return "result";
    }
	
}
