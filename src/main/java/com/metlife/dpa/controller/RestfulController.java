package com.metlife.dpa.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.dpa.model.Product;
import com.metlife.dpa.service.IProductService;
import com.metlife.dpa.service.ProductService;


@RestController
public class RestfulController {
	@Autowired
	IProductService service;
	@RequestMapping("/products")
	public List<Product> getProducts(){
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Backpack", BigDecimal.valueOf(79.99)));
		products.add(new Product("Shirt", BigDecimal.valueOf(49.99)));
		products.add(new Product("Running Shoes", BigDecimal.valueOf(89.99)));
		ProductService ps;

		return products;
	}
	
	@RequestMapping(value="/products", method=RequestMethod.POST)
	public Product addProduct(@RequestBody Product product){
		return product;
	}

}
