<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dog Form</title>
</head>
<body>
      
<div class="container" style="float:left; margin-left:30px;">
	<div class="row">
        <!-- panel preview -->
        <form action="/CapstoneDemo/app/result" method="POST">
        <div class="col-sm-5">
            <h4>Add Dog:</h4>
            <div class="panel panel-default">
                <div class="panel-body form-horizontal payment-form">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="age" class="col-sm-3 control-label">Age</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="age" name="age">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="breed" class="col-sm-3 control-label">Breed</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="breed" name="breed">
                        </div>
                    </div> 
                </div>
            </div>            
     	<div class="col-xs-12">
               <hr style="border:1px dashed #dddddd;">
               <button type="submit" class="btn btn-primary btn-block">
               		<span class="glyphicon glyphicon-plus"></span> Submit
               </button>
       		</div>  
		</div>
		</form>
	</div>
</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

</body>
</html>