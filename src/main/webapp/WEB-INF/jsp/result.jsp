<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<title>Dog Form Result</title>
</head>
<body>
    Dog Submitted
    <br><br>
    <table class="table">
        <tr>
            <td><h2>Name</h2></td>
            <td><h2>Age</h2></td>
            <td><h2>Breed</h2></td>
        </tr>
        
        <tr>
            <div class="col-md-4"><td>${ dog.name }</td></div>
            <div class="col-md-4"><td>${ dog.age }</td></div>
            <div class="col-md-4"><td>${ dog.breed }</td></div>
        </tr>
    </table>
    
    <div style="margin-left:120px;"> <img src="http://www.amusingtime.com/images/016/funny-dog-i-dunno-lol.jpg" alt="Cute Dog" height="600" width="900"></div>
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
   
</body>
</html>